package ro.tuc.dsrl.ds.handson.assig.three.consumer.start;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Date;

import ro.tuc.dsrl.ds.handson.assig.three.consumer.connection.QueueServerConnection;
import ro.tuc.dsrl.ds.handson.assig.three.queue.communication.DVD;

public class NewClient {

	private static final String PATH = "logs/";

	private NewClient() {
	}

	public static void main(String[] args) {
		QueueServerConnection queue = new QueueServerConnection("localhost", 8888);

		String message;
		int i = 0;
		DVD dvd;
		File file;
		FileOutputStream fileOutStream;
		OutputStreamWriter outStream;
		Writer out;

		while (true) {
			try {
				message = queue.readMessage();
				file = new File(PATH + "message_" + i + ".txt");
				i++;
				fileOutStream = new FileOutputStream(file);
				outStream = new OutputStreamWriter(fileOutStream);
				out = new BufferedWriter(outStream);
				out.write(new Date().toString());
				out.write("\n\r");
				out.write(message);
				out.close();

				dvd = queue.readDVD();
				file = new File(PATH + "message_" + i + ".txt");
				i++;
				fileOutStream = new FileOutputStream(file);
				outStream = new OutputStreamWriter(fileOutStream);
				out = new BufferedWriter(outStream);
				out.write(new Date().toString());
				out.write("\n\r");
				out.write(dvd.toString());
				out.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
